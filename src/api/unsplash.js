import axios from 'axios';
 
export default  axios.create({
        baseURL: 'https://api.unsplash.com/search/photos',
       headers: {
           Authorization:
            'Client-ID f68cf2a7b80af7f2369837f2a0835dbedd6f5fd46af653681c5f7e8f42e85e70'
         }
    } );
  